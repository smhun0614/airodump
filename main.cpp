#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include "mac.h"
#include <map>
#include <string>
#include <stdlib.h>
#include <iostream>

using namespace std;



struct Content{
	int Beacons; 
	string ESSID;
};


struct Radiotap{
	uint8_t _version;
	uint8_t _pad;
	uint16_t _len;
	uint32_t _present;
};

struct Beacon{
	uint16_t _version;
	uint16_t _pad;
	Mac _da;
	Mac _sa;
	Mac _bssid;

	uint8_t dummy[14];
};

struct Tagged{
	uint8_t _id;
	uint8_t _len;
};

map <string, Content> bssid;

void usage() {
	printf("syntax : airodump <interface>\n");
	printf("sample : airodump mon0\n");
}

void print_beacontable() {
	system("clear");
	printf("BSSID\t\t\tBeacons\t\tESSID\n\n");
	for(auto _bssid : bssid) {
		cout << (string)_bssid.first << '\t' << _bssid.second.Beacons << "\t\t" << (string)_bssid.second.ESSID << "\t\n";
	}
}

int main(int argc, char* argv[]) {
	if (argc != 2) {
        usage();
        return -1;
    }
	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", dev, errbuf);
		return -1;
	}

	while (true) {
		struct pcap_pkthdr* header;
		const u_char* packet;
		int res = pcap_next_ex(handle, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
			break;
		}

		Radiotap* radiotap = (Radiotap*)packet;
		Beacon* beacon = (Beacon*)(packet + (radiotap->_len));
		Tagged* tp = (Tagged*)(packet + radiotap->_len + sizeof(Beacon));

		char *ssid = (char *)tp + 2;
		string essid;
		for(auto i = tp -> _len; i--; )
			essid += *ssid++;

		if((beacon->_version) == 0x0080) {
			auto it = bssid.find(string(beacon->_bssid));
			if(it == bssid.end())bssid.insert({string(beacon->_bssid),{1, essid}}); 
			else (it->second.Beacons)++;
			print_beacontable();
		}
	}

	pcap_close(handle);
}
